<?php
  define('THEME_DIR', get_template_directory_uri());
  define('PROD', true);

  $cssPath = THEME_DIR . '/css/style.css';
  $jsPath = 'http://localhost:8080/transformed.js';

  if (PROD) {
    $cssPath = THEME_DIR . '/css/style.min.css';
    $jsPath = THEME_DIR . '/build/transformed-mini.js';
  }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-68530586-2"></script>
      <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-68530586-2');
      </script>
    <meta name="fragment" content="!">
    <meta charset="utf-8">
    <title>Wikicreation</title>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEME_DIR ?>/icons/apple-touch-icon.png?v=1">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo THEME_DIR ?>/icons/favicon-32x32.png?v=1">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo THEME_DIR ?>/icons/favicon-16x16.png?v=1">
    <link rel="manifest" href="<?php echo THEME_DIR ?>/icons/manifest.json?v=1">
    <link rel="mask-icon" href="<?php echo THEME_DIR ?>/icons/safari-pinned-tab.svg?v=1" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Muli:300,400,700" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo $cssPath ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_DIR ?>/css/print.css" media="print">
  </head>
  <body>
    <div id="root"></div>
    <script type="text/javascript" src="<?php echo $jsPath ?>"></script>
    <script type="text/javascript" src="<?php echo THEME_DIR . '/js/jquery.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo THEME_DIR . '/js/print.js' ?>"></script>
  </body>
</html>
